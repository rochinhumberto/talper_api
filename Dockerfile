FROM python:3.6

RUN mkdir -p /var/www/talper_api/
COPY . /var/www/talper_api/
WORKDIR /var/www/talper_api/

# Install packages
RUN pip3 install -r requirements.txt

# Run flask app
EXPOSE 5000
ENV FLASK_APP="run.py" 
ENV FLASK_DEBUG=1 
ENV FLASK_ENV=production 
ENV DATABASE_URL=postgres://postgres:postgres@138.68.16.244:5432/talper 
ENV JWT_SECRET_KEY=T4lp3R
CMD ["flask", "run", "--host=0.0.0.0"]