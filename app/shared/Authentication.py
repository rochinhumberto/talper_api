#app/shared/Authentication
#MIDDLEWARE PARA AUTENTICAR CON TOKEN PARA LAS PETICIONES
import jwt
import os
import datetime
from flask import json, Response, request, g
from functools import wraps
from ..models.PersonModel import PersonModel

class Auth():
    @staticmethod
    def generate_token(p_id):
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
                'iat': datetime.datetime.utcnow(),
                'sub': p_id
            }
            return jwt.encode(
                payload,
                os.getenv('JWT_SECRET_KEY'),
                'HS256'
            ).decode("utf-8")
        except Exception as e:
            return Response(
                mimetype="application/json",
                response=json.dumps({'error': 'Error generando token'}),
                status=401
            )

    @staticmethod
    def decode_token(token):
        res = {'data': {}, 'error': {}}
        try:
            payload = jwt.decode(token, os.getenv('JWT_SECRET_KEY'))
            res['data'] = {'p_id': payload['sub']}
            return res
        except jwt.ExpiredSignatureError as e1:
            res['error'] = {'mensaje': 'Token expiró, loguee de nuevo'}
            return res
        except jwt.InvalidTokenError:
            res['error'] = {'mensaje': 'Token invalido, intente con otro'}
            return res

    # decorator
    @staticmethod
    def auth_required(func):
        @wraps(func)
        def decorated_auth(*args, **kwargs):
            if 'Authorization' not in request.headers:
                return Response(
                    mimetype="application/json",
                    response=json.dumps({'error': 'Se requiere token de autorizacion, logueate para obtener uno'}),
                    status=401
                )
            token = request.headers.get('Authorization')
            data = Auth.decode_token(token)
            if data['error']:
                return Response(
                    mimetype="application/json",
                    response=json.dumps(data['error']),
                    status=401
                )
              
            p_id = data['data']['p_id']      
            g.user = {'id': p_id}
            return func(*args, **kwargs)
        return decorated_auth
