#app/app.py

from flask import Flask, jsonify
from werkzeug.exceptions import HTTPException, BadRequest, InternalServerError

from .config import app_config
from .models.PersonModel import db

# import user_api blueprint
from .resources.Person import person_api as person_apis

def create_app(env_name):
    
    # app initiliazation
    app = Flask(__name__)

    app.config.from_object(app_config[env_name])

    # initializing db
    db.init_app(app)

    app.register_blueprint(person_apis, url_prefix='/api/v1/persona')

    @app.route('/', methods=['GET']) #ENDPOINT DEFAULT
    def index():
        return 'API PRUEBA TALPER Humberto Rochin Macias (rochinhumberto@gmail.com) '    

    @app.errorhandler(Exception) #MANEJADOR DE ERRORES
    def handle_error(e):
        code = 500
        if isinstance(e, HTTPException):
            code = e.code
        return jsonify(error=str(e)), code

    return app