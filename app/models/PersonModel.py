# app/models/PersonModel.py
from flask import Flask
from marshmallow import fields, Schema
from flask_sqlalchemy import SQLAlchemy

# initialize our db
db = SQLAlchemy()

class PersonModel(db.Model):
    # table name
    __tablename__ = 'personas'

    p_id = db.Column(db.Integer, db.Sequence('personas_p_id_seq'), primary_key=True)
    nombre = db.Column(db.String(50), nullable=False)
    fecha_nacimiento = db.Column(db.Date(), nullable=False)
    puesto = db.Column(db.String(50), nullable=False)

    # class constructor
    def __init__(self, nombre, fecha_nacimiento, puesto):
        self.nombre = nombre
        self.fecha_nacimiento = fecha_nacimiento
        self.puesto = puesto

    def add(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def update(self):  
        db.session.commit()

class PersonSchema(Schema):
    p_id = fields.Integer(dump_only=True)
    nombre = fields.String(required=True)
    fecha_nacimiento = fields.Date(required=True)
    puesto = fields.String(required=True)