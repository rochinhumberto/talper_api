#/app/resources/Person

from flask import request, json, Response, Blueprint, jsonify
from ..models.PersonModel import PersonModel, PersonSchema
from ..shared.Authentication import Auth

person_api = Blueprint('person_api', __name__)
persons_schema = PersonSchema(many=True)
person_schema = PersonSchema()


@person_api.route('/', methods=['GET']) #OBTENER TODAS LAS PERSONAS
@Auth.auth_required
def get_all():
    persons = PersonModel.query.all()
    persons_data = person_schema.dump(persons, many=True)
    return custom_response(persons_data, 200)


@person_api.route('/<int:p_id>', methods=['GET']) #OBTENER PERSONA POR ID
@Auth.auth_required
def get_a_person(p_id):

    person = PersonModel.query.get(p_id)
    if not person:
        return custom_response({'error': 'Persona no encontrada'}, 404)
    
    per_data = person_schema.dump(person)
    return custom_response(per_data, 200)


@person_api.route('/', methods=['POST'], strict_slashes=False) #AGREGAR PERSONA
@Auth.auth_required
def create():

    req_data = request.get_json()
    print(req_data)
    if not req_data:
        return custom_response({'error': 'Json incorrecto'}, 400)
    nombre=req_data.get('nombre')
    if not nombre:
        return custom_response({'error': 'Falta el campo nombre'}, 400)
    fecha_nacimiento=req_data.get('fecha_nacimiento')
    if not fecha_nacimiento:
        return custom_response({'error': 'Falta el campo fecha_nacimiento'}, 400)
    puesto=req_data.get('puesto')
    if not puesto:
        return custom_response({'error': 'Falta el campo puesto'}, 400)
    #data, error = persons_schema.load(req_data)
    #if error:
    #    return custom_response(jsonify(error), 400)

    person = PersonModel(
            nombre = nombre,
            fecha_nacimiento = fecha_nacimiento,
            puesto = puesto
        )

    person.add()

    result = person_schema.dump(person)
    return custom_response(result, 201)


@person_api.route('/<int:p_id>', methods=['PUT']) #ACTUALIZA PERSONA
@Auth.auth_required
def update(p_id):

    req_data = request.get_json()
    person = PersonModel.query.get(p_id)
    if not person:
        return custom_response({'error': 'Persona no encontrada'}, 404)
    if not req_data:
        return custom_response({'error': 'Json incorrecto'}, 400)
    nombre=req_data.get('nombre')
    if not nombre:
        return custom_response({'error': 'Falta el campo nombre'}, 400)
    fecha_nacimiento=req_data.get('fecha_nacimiento')
    if not fecha_nacimiento:
        return custom_response({'error': 'Falta el campo fecha_nacimiento'}, 400)
    puesto=req_data.get('puesto')
    if not puesto:
        return custom_response({'error': 'Falta el campo puesto'}, 400)
    #data, error = person_schema.load(req_data, partial=True)
    #if error:
    #    return custom_response(jsonify(error), 400)

    person.nombre = nombre
    person.fecha_nacimiento = fecha_nacimiento
    person.puesto = puesto

    person.update()

    result = person_schema.dump(person)
    return custom_response(result, 200)



@person_api.route('<int:p_id>', methods=['DELETE']) #ELIMINAR REGISTRO DE PERSONA POR ID
@Auth.auth_required
def delete(p_id):

    person = PersonModel.query.get(p_id)
    if not person:
        return custom_response({'error': 'Persona no encontrada'}, 404)
    person.delete()
    result = person_schema.dump(person)
    return custom_response({'mensaje': 'Registro de persona eliminado'}, 200)


@person_api.route('/gettoken', methods=['POST']) #GENERA TOKENP PARA PETICIONES
def gettoken():
    req_data = request.get_json()

    token = Auth.generate_token(100)
    return custom_response({'token': token}, 200)


def custom_response(res, status_code): #RESPUESTA PERSONALIZADA
    return Response(
        mimetype="application/json",
        response=json.dumps(res),
        status=status_code
    )
