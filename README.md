# talper_api

API para test de Talper  

Requiere:  

Python 3.6.9  
PostgreSQL 10.14  
Docker 19.03.13  
docker-compose 1.27.4  
Flask 1.1.1  

SE INSTALAN LOS COMPONENTES CON EL METODO:  
pip3 install -r requirements.txt  

VARIABLES:  
export FLASK_APP=run.py  
export FLASK_ENV=development  
export DATABASE_URL=postgres url string  
export JWT_SECRET_KEY=As3cr3t  

DESPLIEGA:  
flask run --host=0.0.0.0  